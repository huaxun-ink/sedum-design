var express = require('express')
var ejs = require('ejs')
var fs = require('fs')
var router = express.Router()
var utils = require('../utils')
/* GET home page. */
router.post('/', async function (req, res, next) {
    try {
        var filePath = '../src/views/' + req.body.moduleName
        if (!fs.existsSync(filePath)) {
            fs.mkdirSync(filePath)
        }
        // 在一个.vue文件中生成代码
        if (req.body.method == 1) {
            await utils.generate('./views/listAndEdit.ejs', filePath + '/listPage.vue', {
                moduleName: utils.firstToUpper(req.body.moduleName),
                originalName: req.body.moduleName,
                moduleNameZh: req.body.moduleNameZh,
                authorityId: req.body.authorityId,
                formList: req.body.formList,
            })
            await utils.generate('./views/router.ejs', filePath + '/router.js', {
                moduleName: req.body.moduleName,
                moduleNameZh: utils.firstToUpper(req.body.moduleNameZh),
                authorityId: req.body.authorityId,
            })
        } else if(req.body.method == 2){ // 在多个.vue文件中生成代码
            await utils.generate('./views/list.ejs', filePath + '/listPage.vue', {
                moduleName: utils.firstToUpper(req.body.moduleName),
                originalName: req.body.moduleName,
                moduleNameZh: req.body.moduleNameZh,
                authorityId: req.body.authorityId,
                formList: req.body.formList,
            })
            await utils.generate('./views/edit.ejs', filePath + '/editPage.vue', {
                moduleName: utils.firstToUpper(req.body.moduleName),
                originalName: req.body.moduleName,
                moduleNameZh: req.body.moduleNameZh,
                authorityId: req.body.authorityId,
                formList: req.body.formList,
            })
            await utils.generate('./views/routerOther.ejs', filePath + '/router.js', {
                moduleName: req.body.moduleName,
                moduleNameZh: utils.firstToUpper(req.body.moduleNameZh),
                authorityId: req.body.authorityId,
            })
        }
        await utils.generate('./views/api.ejs', '../src/api/' + req.body.moduleName + '.js', {
            moduleName: utils.firstToUpper(req.body.moduleName),
            moduleNameZh: utils.firstToUpper(req.body.moduleNameZh),
            authorityId: req.body.authorityId,
        })
        res.send({
            code: 200,
            msg: '生成成功',
        })
    } catch (error) {
        console.log(error)
        res.send({
            code: 500,
            msg: error,
        })
    }
})

module.exports = router
