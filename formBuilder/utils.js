var ejs = require('ejs')
var fs = require('fs')
module.exports = {
    /**
     * 转换成驼峰
     * @param {String} str 需要转换的字符
     */
    firstToUpper: (str) => {
        return str.trim().replace(str[0], str[0].toUpperCase())
    },
    /**
     * 用Ejs模板生成代码文件
     * @param {String} path 模板路径
     * @param {String} fileName 生成路径+文件名称
     * @param {Object} data 给模板传递的数据
     */
    generate: (path, fileName, data) => {
        return new Promise((resolve, reject) => {
            ejs.renderFile(path, data, function (error, str) {
                if (error) {
                    reject(error)
                } else {
                    fs.writeFile(fileName, str, function (err) {
                        if (err) {
                            reject(err)
                        } else {
                            resolve('数据写入成功！')
                        }
                    })
                }
            })
        })
    },
}
