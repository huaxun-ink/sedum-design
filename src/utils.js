import _ from 'lodash'
import { useStore } from './store'
import router from './router'

const store = useStore()

export const closeNowTab = () => {
    const tabList = [...document.querySelectorAll('.tab-item.TAB-UNIQUE')]
    const tabIndex = tabList.findIndex((item) => item == document.querySelector('.tab-item.TAB-UNIQUE.active'))
    if (tabIndex != -1) {
        if (router.currentRoute.value.fullPath == store.routerStack[tabIndex].path) {
            if (store.routerStack[tabIndex + 1]) {
                router.push(store.routerStack[tabIndex + 1])
            } else {
                router.push(store.routerStack[tabIndex - 1])
            }
        }
        store.routerStackRemove(tabIndex)
    }
}

export const firstPath = () => {
    let finded
    function deepFn(data) {
        for (let index = 0; index < data.length; index++) {
            if (finded) break
            if (!data[index].children && data[index].path) {
                finded = data[index]
                break
            }
            if (data[index].children && data[index].children.length) {
                deepFn(data[index].children)
            }
        }
    }
    deepFn(store.menuList)
    return finded?.path
}

export const filterCurrentItem = (originalData, itemId) => {
    let treeData = _.cloneDeep(originalData)
    function loop(data, disabled) {
        for (let index = 0; index < data.length; index++) {
            // 如果是当前节点或者子节点将全部禁用
            if (data[index].id == itemId || disabled) {
                data[index].disabled = true
                if (data[index].children) {
                    loop(data[index].children, true)
                }
            } else if (data[index].children) {
                loop(data[index].children)
            }
        }
    }
    itemId ? loop(treeData) : null
    return treeData
}

export const firstToUpper = (str) => {
    return str.trim().replace(str[0], str[0].toUpperCase())
}

// 过滤脱敏项
export const filterDesensitizationItems = (data) => {
    let newData = {}
    for (const key in data) {
        const str = String(data[key])
        if (str.indexOf('*') == -1) {
            newData[key] = data[key]
        }
    }
    return newData
}
