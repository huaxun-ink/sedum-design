import { createApp } from 'vue'
import Antd from 'ant-design-vue'
import './iconfont/iconfont.css'
import 'animate.css'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
createApp(App).use(createPinia()).use(router).use(Antd).mount('#app')
