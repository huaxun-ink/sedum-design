import Home from '../views/home/router'
import Statistics from '../views/statistics/router'
import Tool from '../views/tool/router'
export default [
    {
        path: '/',
        name: 'main',
        redirect: '/login',
    },
    {
        path: '/login',
        name: 'login',
        meta: {
            // 是否被tab忽略
            ignoredByTab: true,
        },
        component: () => import('../views/main/loginPage.vue'),
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'notFound',
        meta: {
            // 是否被tab忽略
            ignoredByTab: true,
        },
        component: () => import('../views/main/notFound.vue'),
    },
    {
        path: '/uniappGenerator',
        name: 'uniapp低代码平台',
        meta: {
            name: 'uniappGenerator',
            // 关闭默认背景
            background: 'off',
        },
        component: () => import('../views/uniappGenerator/indexPage.vue'),
    },
    Home,
    Statistics,
    Tool,
]
