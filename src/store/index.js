import { defineStore } from 'pinia'
import { baseUrl } from '../../config'

export const useStore = defineStore('main', {
    state: () => {
        return {
            refreshKey: 1, // 用于刷新主视图
            spinning: false,
            token: localStorage.getItem('token'),
            baseUrl: baseUrl,
            menuList: JSON.parse(localStorage.getItem('menuList') || '[]'),
            buttonMap: JSON.parse(localStorage.getItem('buttonMap') || '{}'),
            selectedKeys: localStorage.getItem('selectedKeys') ? JSON.parse(localStorage.getItem('selectedKeys')) : [],
            openKeys: localStorage.getItem('openKeys') ? JSON.parse(localStorage.getItem('openKeys')) : [],
            userId: localStorage.getItem('userId'),
            siderTheme: 'light', // dark | light
            routerStack: JSON.parse(localStorage.getItem('routerStack') || '[]'),
        }
    },
    actions: {
        setToken(token) {
            this.token = token
            localStorage.setItem('token', token)
        },
        setUserId(id) {
            this.userId = id
            localStorage.setItem('userId', id)
        },
        setSelectedKeys(selectedKeys) {
            this.selectedKeys = selectedKeys
            localStorage.setItem('selectedKeys', JSON.stringify(selectedKeys))
        },
        setOpenKeys(openKeys) {
            this.openKeys = openKeys
            localStorage.setItem('openKeys', JSON.stringify(openKeys))
        },
        setMenuList(menuList) {
            this.menuList = menuList
            localStorage.setItem('menuList', JSON.stringify(menuList))
        },
        setButtonMap(buttonMap) {
            this.buttonMap = buttonMap
            localStorage.setItem('buttonMap', JSON.stringify(buttonMap))
        },
        routerStackAdd(routeItem) {
            if (routeItem) this.routerStack.push(routeItem)
            localStorage.setItem('routerStack', JSON.stringify(this.routerStack))
        },
        routerStackRemove(index) {
            this.routerStack.splice(index, 1)
            localStorage.setItem('routerStack', JSON.stringify(this.routerStack))
        },
        logout() {
            this.setToken('')
            this.setUserId('')
            this.setMenuList([])
            this.setButtonMap({})
        },
    },
})
