import * as http from './request'

/**
 * @description: 获取菜单列表
 */
export function getMenuList(params) {
    return http.apiGet('/sys/menu', params)
}

/**
 * @description: 获取菜单详情
 */
export function getMenuDetail(id) {
    return http.apiGet('/sys/menu/' + id)
}

/**
 * @description: 删除菜单
 */
export function deleteMenu(id) {
    return http.apiDelete('/sys/menu/' + id)
}

/**
 * @description: 修改菜单
 */
export function editMenu(params) {
    return http.apiPut('/sys/menu', params)
}

/**
 * @description: 新增菜单
 */
export function addMenu(params) {
    return http.apiPost('/sys/menu', params)
}
