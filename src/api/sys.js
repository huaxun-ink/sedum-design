import * as http from './request'

/**
 * @description: 字典类型列表
 */
export function dictTypeList(params) {
    return http.apiGet('/sys/dict/type', params)
}
/**
 * @description: 获取oss凭证
 */
export function getOssToken() {
    return http.apiGet('/aliyun/oss/token')
}
/**
 * @description: 翻译
 */
export function translate(content) {
    return http.apiGet('/sys/server/form/translation/list?content=' + content)
}
