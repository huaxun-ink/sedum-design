import * as http from './request'

/**
 * @description: 获取角色列表
 */
export function getRoleList(params) {
    return http.apiGet('/sys/role', params)
}

/**
 * @description: 获取所有角色列表
 */
export function getAllRoleList(params) {
    return http.apiGet('/sys/role/all', params)
}

/**
 * @description: 获取角色详情
 */
export function getRoleDetail(id) {
    return http.apiGet('/sys/role/' + id)
}

/**
 * @description: 修改角色
 */
export function editRole(params) {
    return http.apiPut('/sys/role', params)
}

/**
 * @description: 新增角色
 */
export function addRole(params) {
    return http.apiPost('/sys/role', params)
}

/**
 * @description: 删除角色
 */
export function deleteRole(id) {
    return http.apiDelete('/sys/role/' + id)
}

/**
 * @description: 数据权限
 */
export function roleScope(params) {
    return http.apiPut('/sys/role/data/scope', params)
}
