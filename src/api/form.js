import * as http from './request'

/**
 * @description: 获取表单列表
 */
export function getFormList(params) {
    return http.apiGet('/sys/server/form', params)
}

/**
 * @description: 获取服务端目录
 */
export function getPathList() {
    return http.apiGet('/sys/server/form/path/list')
}

/**
 * @description: 获取表单详情
 */
export function getFormDetail(id) {
    return http.apiGet('/sys/server/form/' + id)
}

/**
 * @description: 删除表单
 */
export function deleteForm(id) {
    return http.apiDelete('/sys/server/form/' + id)
}

/**
 * @description: 修改表单
 */
export function editForm(params) {
    return http.apiPut('/sys/server/form', params)
}

/**
 * @description: 新增表单
 */
export function addForm(params) {
    return http.apiPost('/sys/server/form', params)
}
