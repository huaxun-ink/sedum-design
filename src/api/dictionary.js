import * as http from './request'

/**
 * @description: 获取字典列表
 */
export function getDictionaryList(params) {
    return http.apiGet('/sys/dict/type', params)
}

/**
 * @description: 获取字典详情
 */
export function getDictionaryDetail(id) {
    return http.apiGet('/sys/dict/type/' + id)
}

/**
 * @description: 删除字典
 */
export function deleteDictionary(id) {
    return http.apiDelete('/sys/dict/type/' + id)
}

/**
 * @description: 修改字典
 */
export function editDictionary(params) {
    return http.apiPut('/sys/dict/type', params)
}

/**
 * @description: 新增字典
 */
export function addDictionary(params) {
    return http.apiPost('/sys/dict/type', params)
}
