import * as http from './request'

/**
 * @description: 用户登录
 */
export function login(params) {
    return http.apiPost('/login', params)
}
/**
 * @description: 用户注册
 */
export function register(params) {
    return http.apiPost('/register', params)
}
/**
 * @description: 重置密码
 */
export function resetPassword(params) {
    return http.apiPut('/sys/user/reset/password', params)
}
/**
 * @description: 修改密码
 */
export function changePassword(params) {
    return http.apiPut('/sys/user/change/password', params)
}
/**
 * @description: 用户详情
 */
export function userInfo() {
    return http.apiGet('/sys/user/own')
}

/**
 * @description: 获取验证码
 */
export function getCode() {
    return http.apiGet('/captcha')
}

/**
 * @description: 获取用户列表
 */
export function getUserList(params) {
    return http.apiGet('/sys/user', params)
}

/**
 * @description: 获取用户详情
 */
export function getUserDetail(id) {
    return http.apiGet('/sys/user/' + id)
}

/**
 * @description: 修改用户
 */
export function editUser(params) {
    return http.apiPut('/sys/user', params)
}

/**
 * @description: 新增用户
 */
export function addUser(params) {
    return http.apiPost('/sys/user', params)
}

/**
 * @description: 删除用户
 */
export function deleteUser(id) {
    return http.apiDelete('/sys/user/' + id)
}
