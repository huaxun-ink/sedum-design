import * as http from './request'

/**
 * @description: 获取组织列表
 */
export function getOrgList(params) {
    return http.apiGet('/sys/organization', params)
}

/**
 * @description: 获取组织详情
 */
export function getOrgDetail(id) {
    return http.apiGet('/sys/organization/' + id)
}

/**
 * @description: 删除组织
 */
export function deleteOrg(id) {
    return http.apiDelete('/sys/organization/' + id)
}

/**
 * @description: 修改组织
 */
export function editOrg(params) {
    return http.apiPut('/sys/organization', params)
}

/**
 * @description: 新增组织
 */
export function addOrg(params) {
    return http.apiPost('/sys/organization', params)
}
