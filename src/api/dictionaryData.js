import * as http from './request'

/**
 * @description: 获取字典数据列表
 */
export function getDictionaryDataList(params) {
    return http.apiGet('/sys/dict/data', params)
}

/**
 * @description: 获取所有字典数据列表
 */
export function getDictionarylistAll(type) {
    return http.apiGet('/sys/dict/data/listAll?dictType=' + type)
}

/**
 * @description: 获取字典数据详情
 */
export function getDictionaryDataDetail(id) {
    return http.apiGet('/sys/dict/data/' + id)
}

/**
 * @description: 删除字典数据
 */
export function deleteDictionaryData(id) {
    return http.apiDelete('/sys/dict/data/' + id)
}

/**
 * @description: 修改字典数据
 */
export function editDictionaryData(params) {
    return http.apiPut('/sys/dict/data', params)
}

/**
 * @description: 新增字典数据
 */
export function addDictionaryData(params) {
    return http.apiPost('/sys/dict/data', params)
}
