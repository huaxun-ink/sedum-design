import axios from 'axios'
import { baseUrl } from '../../config'
import router from '../router'
import { useStore } from '../store'
import { message } from 'ant-design-vue'
import { aes } from './cryptoFun'

const encrypt = 0 // 加密全局开关const ifErrorMsg = 1 // 是否弹出错误提示信息  0是1否
// 跟路径配置
axios.defaults.baseURL = baseUrl

//防止重复弹窗
message.config({
    maxCount: 1,
})

// 请求超时时间
axios.defaults.timeout = 100000

//请求头
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8' //配置请求头
axios.defaults.headers.put['Content-Type'] = 'application/json;charset=UTF-8' //配置请求头

// 请求拦截器
axios.interceptors.request.use(
    (config) => {
        const store = useStore()
        config.headers.token = store.token
        config.headers.post['token'] = store.token
        return config
    },
    (error) => {
        return Promise.reject(error)
    }
)

// 响应拦截器
axios.interceptors.response.use(
    (response) => {
        const store = useStore()
        if (response.data.code === 200) {
            if (response.headers.token) {
                store.setToken(response.headers.token)
            }
            return Promise.resolve(response)
        } else {
            switch (response.data.code) {
                // 401: 未登录
                case 401:
                    message.error(response.data.message)
                    if (router.currentRoute.value.fullPath != '/') {
                        message.error(response.data.message)
                        store.logout()
                        router.replace({ path: '/login' })
                    }
                    break
                // 无权限
                case 403:
                    // message.error(response.data.message)
                    message.error('无操作权限!')
                    break
                // 404请求不存在
                // case 404:
                //     message.error('网络请求不存在')
                //     break                // 409后端正常提示                case 409:                    message.error(response.data.message || response.data.msg)                    break                case 400:                    message.error(response.data.message || response.data.msg)                    break
                // 其他错误，直接抛出错误提示
                default:                    if(ifErrorMsg) {                        message.error('网络开小差了,请稍后再试!')                    }else{                        message.error(response.data.message || response.data.msg)                    }
            }
            return Promise.resolve(response)
        }
    },
    // 服务器状态码不是200的情况
    (error) => {
        const store = useStore()
        store.spinning = false
        message.error('网络开小差了,请稍后再试!')
        console.log(error)
        return Promise.reject(error.response)
    }
)
/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function apiGet(url, params) {
    return new Promise((resolve, reject) => {
        axios
            .get(url, {
                params: params,
            })
            .then((res) => {
                resolve(res.data)
            })
            .catch((err) => {
                return reject(err)
            })
    })
}
/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function apiPost(url, params) {
    if (encrypt == 1) params = aes.en(JSON.stringify(params))
    return new Promise((resolve, reject) => {
        axios
            .post(url, params)
            .then((res) => {
                resolve(res.data)
            })
            .catch((err) => {
                return reject(err)
            })
    })
}
/**
 * put方法，对应put请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function apiPut(url, params) {
    if (encrypt == 1) params = aes.en(JSON.stringify(params))
    return new Promise((resolve, reject) => {
        axios
            .put(url, params)
            .then((res) => {
                resolve(res.data)
            })
            .catch((err) => {
                return reject(err)
            })
    })
}
/**
 * delete方法，对应delete请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function apiDelete(url, params) {
    return new Promise((resolve, reject) => {
        axios
            .delete(url, params)
            .then((res) => {
                resolve(res.data)
            })
            .catch((err) => {
                return reject(err)
            })
    })
}
