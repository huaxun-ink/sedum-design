export default {
    path: '/tool',
    name: '系统工具',
    redirect: '/tool/serverCodeGenerator',
    component: () => import('@/views/main/mainPage.vue'),
    children: [
        {
            path: 'serverCodeGenerator',
            name: '后端代码生成',
            component: () => import('./serverCodeGenerator.vue'),
        },
        {
            path: 'frontEndCodeGenerator',
            name: '前端代码生成',
            component: () => import('./frontEndCodeGenerator.vue'),
        },
        {
            path: 'mapDemo',
            name: '地图示例',
            component: () => import('./mapDemo.vue'),
        },
        {
            path: 'iconDemo',
            name: '图标示例',
            component: () => import('./iconDemo.vue'),
        },
        {
            path: 'richTextDemo',
            name: '富文本示例',
            component: () => import('./richTextDemo.vue'),
        },
        {
            path: 'ossDemo',
            name: 'OSS示例',
            component: () => import('./ossDemo.vue'),
        },
        {
            path: 'autoTest',
            name: '自动化测试',
            component: () => import('./autoTest.vue'),
        },
    ],
}
