export default {
    path: '/statistics',
    name: '统计分析',
    redirect: '/statistics/overview',
    component: () => import('@/views/main/mainPage.vue'),
    children: [
        {
            path: 'overview', //菜单管理
            name: '总体概览',
            meta: {
                name: 'overview',
                // 关闭默认背景
                background: 'off',
            },
            component: () => import('./overviewBox.vue'),
        },
        {
            path: 'sunzi', //孙子
            name: '孙子',
            meta: {
                name: 'sunzi',
            },
            component: () => import('./sunziBox.vue'),
        },
    ],
}
