export default {
    path: '/Home',
    name: '系统管理',
    component: () => import('@/views/main/mainPage.vue'),
    redirect: '/Home/menuManagement',
    children: [
        {
            path: 'menuManagement', //菜单管理
            name: '菜单管理',
            meta: {
                name: 'menuManagement',
            },
            component: () => import('./menuManagement.vue'),
        },
        {
            path: 'userManagement', //用户管理
            name: '用户管理',
            meta: {
                name: 'userManagement',
            },
            component: () => import('./userManagement.vue'),
        },
        {
            path: 'roleManagement', //角色管理
            name: '角色管理',
            meta: {
                name: 'roleManagement',
            },
            component: () => import('./roleManagement.vue'),
        },
        {
            path: 'departmentManagement', //部门管理
            name: '部门管理',
            meta: {
                name: 'departmentManagement',
            },
            component: () => import('./departmentManagement.vue'),
        },
        {
            path: 'dictionaryManagement', //字典管理
            name: '字典管理',
            meta: {
                name: 'dictionaryManagement',
            },
            component: () => import('./dictionaryManagement.vue'),
        },
        {
            path: 'dictionaryDataManagement', //字典管理
            name: '字典数据管理',
            meta: {
                name: 'dictionaryDataManagement',
            },
            component: () => import('./dictionaryDataManagement.vue'),
        },
    ],
}
