import { baseUrl } from './config'
import { useStore } from './src/store'

export default class Dialer {
    constructor(
        props = {
            onOpen: () => {},
            onMessage: () => {},
            onClose: () => {},
            onError: () => {},
        }
    ) {
        this.onOpen = props.onOpen
        this.onMessage = props.onMessage
        this.onClose = props.onClose
        this.onError = props.onError
        this.SOCKETURL = 'http://10.0.2.64:12300' + '/websocket'
    }
    socket = null
    init() {
        return new Promise((resolve, reject) => {
            if (typeof WebSocket == 'undefined') {
                throw new Error('您的浏览器不支持WebSocket')
            } else {
                const store = useStore()
                this.SOCKETURL = this.SOCKETURL.replace('https', 'ws').replace('http', 'ws') + '?token=' + store.token
                console.log(this.SOCKETURL)
                this.socket != null ? this.socket.close() : (this.socket = null)
                this.socket = new WebSocket(this.SOCKETURL)
                //打开事件
                this.socket.onopen = () => {
                    this.onOpen()
                    resolve()
                }
                //获得消息事件
                this.socket.onmessage = this.onMessage
                //关闭事件
                this.socket.onclose = this.onClose
                //发生了错误事件
                this.socket.onerror = () => {
                    this.onError()
                    reject()
                }
            }
        })
    }
    async close() {
        //     await this.init()
        this.socket.close()
    }
}
