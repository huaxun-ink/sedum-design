<p align="center">
	<img alt="logo" src="https://hxgk-oss-dev.huaxun.ink/sedum/lQLPJwjA19d-c1Y-RrAeolpf2yjr8ASZv_bHQN8A_70_62_1688468313312.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">sedum-design v4.0.2</h1>
<h4 align="center">基于Vue3 + Ant Design Vue开发的后台管理快速开发框架</h4>

## 平台简介

本项目是基于Vue3 + Ant Design Vue开发的后台管理框架，配合后端框架 [sedum-service](https://gitee.com/huaxun-ink/sedum-service) 使用，可快速搭建一套完整的后台管理系统，代码生成器可满足大多数场景的普通需求，无需人工手写代码，只需配置路由，生成即可使用。可称之为东二环最强代码生成器，系统会陆续更新一些实用功能，欢迎关注 star。

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置，管理用户。
2.  部门管理：配置系统组织机构（公司、部门、小组）。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
13. 前端代码生成：根据设置好的字段、名称、长度、是否必填等，可直接生成vue页面以及请求js文件到项目中。14. 后端代码生成：根据设置好的名称、注释、前缀、生成路径、字段属性等，可直接生成控制器文件到项目中，以及生成数据库表。
## 注意事项1. 下载依赖若耗时太长，可改用 pnpm i 命令。2. 若使用前端代码生成器，则需要开启代码生成器服务，操作方法：   进入项目根目录下formBuilder目录，cmd 运行 npm run start 命令，开启成功即可使用前端代码生成器。
## 演示图

<table>
    <tr>
        <td><img src="https://hxgk-oss-dev.huaxun.ink/sedum/1688442734612_1688467022370.png"/></td>
        <td><img src="https://hxgk-oss-dev.huaxun.ink/sedum/1688442983046_1688467064050.png"/></td>
    </tr>
    <tr>
        <td><img src="https://hxgk-oss-dev.huaxun.ink/sedum/1688443123451_1688467097103.jpg"/></td>
        <td><img src="https://hxgk-oss-dev.huaxun.ink/sedum/1688443151627_1688467124270.png"/></td>
    </tr>
    <tr>
        <td><img src="https://hxgk-oss-dev.huaxun.ink/sedum/1688452389000_1688467146946.png"/></td>
        <td><img src="https://hxgk-oss-dev.huaxun.ink/sedum/1688452453588_1688467169482.png"/></td>
    </tr>
</table>